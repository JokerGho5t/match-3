﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    #region SinglTon
    public static BoardManager Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    [Header("Setup board")]
    public int width;
    public int height;
    public GameObject tilePrefab;
    public GameObject[] dotPrefabs;
    public GameObject[,] allDots;

    private GameObject[,]matches;

    void Start()
    {
        allDots = new GameObject[width, height];
        Generation();
    }

    private void Generation()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector2 tempPos = new Vector2(i, j);
                GameObject backgroundTile = Instantiate(tilePrefab, tempPos, Quaternion.identity, transform);
                backgroundTile.name = "(" + i + "," + j + ")";

                int UseDot = Random.Range(0, dotPrefabs.Length);

                int maxIterations = 0;
                while (MatchesAt(i, j, dotPrefabs[UseDot]) && maxIterations < 100)
                {
                    UseDot = Random.Range(0, dotPrefabs.Length);
                    maxIterations++;
                    Debug.Log(maxIterations);
                }
                maxIterations = 0;

                GameObject dot = Instantiate(dotPrefabs[UseDot], tempPos, Quaternion.identity, backgroundTile.transform);
                dot.name = dotPrefabs[UseDot].tag;
                allDots[i, j] = dot;

            }
        }
    }

    private bool MatchesAt(int column, int row, GameObject dot)
    {
        if(column > 1)
            if (allDots[column - 1, row].tag == dot.tag && allDots[column - 2, row].tag == dot.tag)
                return true;
        if(row > 1)
            if (allDots[column, row - 1].tag == dot.tag && allDots[column, row - 2].tag == dot.tag)
                return true;
        return false;
    }

    private void ShakeBoard()
    {

    }

    #region Find Matches
//    function getMatchHoriz(i, j)

//    temp = 1
//	for j = j ,9 do
//		if matrix[i][j] == matrix[i][j + 1] then

//            temp = temp + 1
//		else
//			return temp
//        end

//    end
//	return temp
//end

//function getMatchVert(i, j)

//    temp = 1
//	for i = i ,9 do
//		if matrix[i][j] == matrix[i + 1][j] then

//            temp = temp + 1
//		else
//			return temp
//        end

//    end
//	return temp
//end
    private bool CheckMatches()
    {
        matches = allDots;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height - 2; j++)
            {
                getMatchHoriz(i,j);
            }
        }

        return false;
    }

    private int getMatchHoriz(int i, int j)
    {
        int temp = 1;

        for(; j < height; j++)
        {

        }

        return temp;
    }
    #endregion

    private void affectAbove()
    {

    }

}
