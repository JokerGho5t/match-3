﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public int width;
    public int height;
    public GameObject tilePrefab;
    public GameObject[] dotPrefabs;
    public GameObject[,] allDots;

    void Start()
    {
        allDots = new GameObject[width, height];
        SetUp();
    }

    private void SetUp()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector2 tempPos = new Vector2(i, j);
                GameObject backgroundTile = Instantiate(tilePrefab, tempPos, Quaternion.identity, this.transform);
                backgroundTile.name = "(" + i + "," + j + ")";

                int UseDot = Random.Range(0, dotPrefabs.Length);
                GameObject dot = Instantiate(dotPrefabs[UseDot], tempPos, Quaternion.identity, this.transform);
                dot.name = "(" + i + "," + j + ")";
                allDots[i, j] = dot;
            }
        }
    }

    private bool MatchesAt(int column, int row, GameObject dot)
    {
        if(column > 1 && row > 1)
        {
            if (allDots[column - 1, row].tag == dot.tag && allDots[column - 2, row].tag == dot.tag)
            {
                return true;
            }
        }

        return false;
    }

}
